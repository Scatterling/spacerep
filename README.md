# Spacerep
A simple memorization tool loosely based on spaced repetition systems.
Initially made to practice reading an esoteric font inspired by [dotsies](https://dotsies.org) using [FIGlet](http://www.figlet.org).

## Caveats
This program currently assumes that:
* FIGlet is installed.
* FONTFILE is set to the path to a figlet font; either global or relative to wherever you run the program from.
* The questions array is populated with words or phrases which will serve as both questions and answers.
  * I used a list of most frequent words provided by the [Corpus of Global Web-Based English](https://www.english-corpora.org/glowbe/).
* MAXANSWERLENGTH is updated to represent one plus the number of characters in the longest question.

## TODO list
* Add an answers array to complement the questions one.
  * Populate questions and answers from input, stdin or a file.
  * Treat lone questions as their own answer; print the question differently so as to be passed to something like figlet.
* Calculate MAXANSWERLENGTH at runtime to fit any question set.
  * Or render it unnecessary with memory allocation.