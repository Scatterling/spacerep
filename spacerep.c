#include <stdio.h>  // For fgets() and printf()
#include <stdlib.h> // For system()
#include <string.h> // For strcspn(), strcpy(), and strcat()
#define FONTFILE ".flf"
#define MAXSCORE 10
/* "chain" is the modifier for consecutive in/correct answers.	*/
#define MAXCHAIN MAXSCORE * 10
/* This defines the number of questions asked per round.	*/
#define SUBSETSIZE 10
/* The score to clear before adding new questions to the set.	*/
#define MAXANSWERLENGTH 17 // 1 +Longest question's length.
/* A randomly chosen score cutoff for "provenness".		*/
#define LEARNEDLIMIT 9
int main(void)							{
/* A list of words, sorted by priority.				*/
 const char *questions[] =					 {
								 };
 const int total = sizeof(questions) / sizeof(questions[0])	 ;
 /* Initialize scores to 0					 */
 short scores[total], chains[total], pauses[total]		 ;
 for(int i=0;i<total;i++)					  {
  scores[i] = 0							  ;
  chains[i] = 0							  ;
  pauses[i] = 0							 ;}
 /* Initialize the active question set				 */
 short subset[SUBSETSIZE], subsetindex				 ;
 /* A place to store answers given				 */
 char answer[MAXANSWERLENGTH]					 ;
 /* A buffer for the system() string				 */
 int figlength = MAXANSWERLENGTH + strlen(FONTFILE) + 40	 ;
 char figprint[figlength] /* For the system array */		 ;
 while(1)							  {
  /* Select a set of questions; this is a work in progress.	  */
  subsetindex = 0						  ;
  for(int i=0;subsetindex<SUBSETSIZE;i++)			   {
   if(scores[i] * chains[i] - pauses[i] < LEARNEDLIMIT)		    {
    subset[subsetindex] = i					    ;
    subsetindex++						  ;}}
  for(int i=0;i<SUBSETSIZE;i++)					   {
   /* Pass question to figlet -- This needs to be replaced.	  */ 
   strcpy(figprint,"figlet -f ")				  ;
   strcat(figprint,FONTFILE)					  ;
   strcat(figprint," \"")					  ;
   strcat(figprint,questions[subset[i]])			  ;
   strcat(figprint,"\" | sed 's/#/█/g'")			  ;
   system(figprint)						  ;
   //printf("%s\n",questions[subset[i]]) /* Debug output */	  ;
   /* Let the user know you're waiting for input		  */
   printf("Answer? ")						  ;
   /* take answer input, compare to question			  */
   if(fgets(answer,MAXANSWERLENGTH,stdin))			   {
    answer[strcspn(answer,"\n")] = '\0'				  ;}
   /* If incorrect, decrement score by 2 (min of 0)		  */
   if(strcmp(questions[subset[i]], answer))			   {
    printf("Nope. It's \"%s\".\n",questions[subset[i]])		   ;
    if(scores[subset[i]] < 2) scores[subset[i]] = 0		   ;
    else scores[subset[i]] -= 2					   ;
    /* If chain is negative, decrement down to min		   */
    if(chains[subset[i]] < 0)					    {
     if(chains[subset[i]] > -MAXCHAIN) chains[subset[i]]--	   ;}
    /* Else subtract from zero					   */
    else chains[subset[i]] = -1					   ;
    while(strcmp(questions[subset[i]], answer))			    {
     printf("Type \"%s\" to continue.\n",questions[subset[i]])	    ;
     if(fgets(answer,MAXANSWERLENGTH,stdin))			     {
      answer[strcspn(answer,"\n")] = '\0'			  ;}}}
   /* If correct, increment score by 1 (up to maxscore)		  */
   else								   {
    printf("That's right!\n")					   ;
    if(scores[subset[i]] < MAXSCORE) scores[subset[i]]++	   ;
    /* If chain is positive, increment up to max		   */
    if(chains[subset[i]] > 0)					    {
     if(chains[subset[i]] < MAXCHAIN) chains[subset[i]]++	   ;}
    /* Bonus points for getting a question right on the first try  */
    else if(!chains[subset[i]])					    {
    scores[subset[i]] += 3	/* from sqrt(LEARNEDLIMIT) */	    ;
    chains[subset[i]] += 3	/* todo: rework this part. */	   ;}
    /* Else add to zero						   */
    else chains[subset[i]] = 1					 ;}}
  /* Some sort of score decay would probably be good?		  */
  for(int i=0;i<total;i++)					  {
   if(scores[i]) pauses[i]++					 ;}
  for(int i=0;i<SUBSETSIZE;i++)					  {
   pauses[subset[i]]=0						;}}
 return 0;							;}
